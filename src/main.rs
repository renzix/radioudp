
use std::net::UdpSocket;

fn main() -> std::io::Result<()> {
    {
        let location = "127.0.0.1:1234";
        let socket = UdpSocket::bind(location)?;
        println!("Binded to udp socket {}",location);
        let mut buf = [0; 100];
        let (amt,_src) = socket.recv_from(&mut buf)?;
        println!("Recieved stuff from the {}", location);
        let buf = &mut buf[..amt];
        buf.reverse();
        println!("Printing the buffer: {:?}",buf);
    }
    Ok(())
}
